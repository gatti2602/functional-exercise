const fibonacci = number => {
  if (number == 0)
    return 0;
  if (number == 1)
    return 1;
  if (number > 1)
    return fibonacci(number-1) + fibonacci(number-2);
  else
    throw new Error('number must be a natural number or 0 ');
};

module.exports = fibonacci;
