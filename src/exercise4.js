const wordCount = sequence => {

    if (sequence == null)
        return null;

    matchedWords = sequence.match(/\w+/g); //Matches a word
    if (matchedWords == null)
        return null;

    return matchedWords.reduce((stats, word) => {
        word = word.toLowerCase();
        if (stats.hasOwnProperty(word)) {
            stats[word] = stats[word] + 1;
        } else {
            stats[word] = 1;
        }
        return stats;
    }, {});
};

module.exports = {
    wordCount,
};
